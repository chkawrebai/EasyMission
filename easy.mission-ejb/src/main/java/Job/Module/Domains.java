package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Domains
 *
 */
@Entity

public class Domains implements Serializable {

	   
	@Id
	private int IdDomain;
	private String DomainName;
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL,mappedBy="Domainsss")
	private List<specialty> Speciality;
	
	public List<specialty> getSpeciality() {
		return Speciality;
	}
	public void setSpeciality(List<specialty> speciality) {
		Speciality = speciality;
	}

	private static final long serialVersionUID = 1L;

	public Domains() {
		super();
	}   
	public int getIdDomain() {
		return this.IdDomain;
	}

	public void setIdDomain(int IdDomain) {
		this.IdDomain = IdDomain;
	}   
	public String getDomainName() {
		return this.DomainName;
	}

	public void setDomainName(String DomainName) {
		this.DomainName = DomainName;
	}   

}
