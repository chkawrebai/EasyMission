package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: specialty
 *
 */
@Entity

public class specialty implements Serializable {

	@Id @GeneratedValue
	private int Id_spacialty;
	private String NomSpacialty;
	private String SousSpcialty;
	  @ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_Domain")
	    private Domains Domainsss;
	  @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL,mappedBy="sp")
		private List<Offre> offres;
	public List<Offre> getOffres() {
		return offres;
	}
	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}
	public String getSousSpcialty() {
		return SousSpcialty;
	}
	public void setSousSpcialty(String sousSpcialty) {
		SousSpcialty = sousSpcialty;
	}
	public Domains getDomainsss() {
		return Domainsss;
	}
	public void setDomainsss(Domains domainsss) {
		Domainsss = domainsss;
	}

	private static final long serialVersionUID = 1L;

	public specialty() {
		super();
	}   
	public int getId_spacialty() {
		return this.Id_spacialty;
	}

	public void setId_spacialty(int Id_spacialty) {
		this.Id_spacialty = Id_spacialty;
	}   
	public String getNomSpacialty() {
		return this.NomSpacialty;
	}

	public void setNomSpacialty(String NomSpacialty) {
		this.NomSpacialty = NomSpacialty;
	}
   
}
