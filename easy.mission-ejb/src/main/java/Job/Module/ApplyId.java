package Job.Module;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
 
@Embeddable
public class ApplyId implements java.io.Serializable{
	  private static final long serialVersionUID = 1L;
	  @ManyToOne
	  @JoinColumn(name="IdOffre",insertable=false,updatable=false)
	private Offre offres;
	  public Offre getOffres() {
		return offres;
	}
	public void setOffres(Offre offres) {
		this.offres = offres;
	}
	public JobSeeker getJobseekers() {
		return jobseekers;
	}
	public void setJobseekers(JobSeeker jobseekers) {
		this.jobseekers = jobseekers;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jobseekers == null) ? 0 : jobseekers.hashCode());
		result = prime * result + ((offres == null) ? 0 : offres.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplyId other = (ApplyId) obj;
		if (jobseekers == null) {
			if (other.jobseekers != null)
				return false;
		} else if (!jobseekers.equals(other.jobseekers))
			return false;
		if (offres == null) {
			if (other.offres != null)
				return false;
		} else if (!offres.equals(other.offres))
			return false;
		return true;
	}
	@ManyToOne
	@JoinColumn(name="IdProvider",insertable=false,updatable=false)
	private JobSeeker jobseekers;
}
