package Job.Module;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Offre
 *
 */
@Entity

public class Offre implements Serializable {

	   
	@Id
	private int idOffre;
	private Date date_expration;
	private int nbr;
	  @ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_sp")
	  
	private specialty sp;
	  @OneToMany(mappedBy = "jobseekers")
	  private Set<Apply> apllayss = new HashSet<Apply>();

	@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="id_jobProvider")
	private JobProvider jobprovider;
	  public Set<Apply> getRoles() {
		return apllayss;
	}
	public void setRoles(Set<Apply> apllayss) {
		this.apllayss = apllayss;
	}
	public specialty getSp() {
		return sp;
	}
	public void setSp(specialty sp) {
		this.sp = sp;
	}
	public JobProvider getJobprovider() {
		return jobprovider;
	}
	public void setJobprovider(JobProvider jobprovider) {
		this.jobprovider = jobprovider;
	}
	public Degree getDgr() {
		return dgr;
	}
	public void setDgr(Degree dgr) {
		this.dgr = dgr;
	}

	@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="id_Degree")
	  
	private Degree dgr;
	private static final long serialVersionUID = 1L;

	public Offre() {
		super();
	}   
	public int getIdOffre() {
		return this.idOffre;
	}

	public void setIdOffre(int idOffre) {
		this.idOffre = idOffre;
	}   
	public Date getDate_expration() {
		return this.date_expration;
	}

	public void setDate_expration(Date date_expration) {
		this.date_expration = date_expration;
	}   
	public int getNbr() {
		return this.nbr;
	}

	public void setNbr(int nbr) {
		this.nbr = nbr;
	}
   
}
