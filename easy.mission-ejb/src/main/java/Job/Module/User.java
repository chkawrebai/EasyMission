package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity

public class User implements Serializable {

	   
	@Id @GeneratedValue
	private int IdUser;
	private String UserName;
	private String tel;
	private String password;
	private String email;
	private String adress;
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL,mappedBy="users")
	private List<media> Medias;
	
	public List<media> getMedias() {
		return Medias;
	}
	public void setMedias(List<media> medias) {
		Medias = medias;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}

	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}   
	public int getIdUser() {
		return this.IdUser;
	}

	public void setIdUser(int IdUser) {
		this.IdUser = IdUser;
	}   
	public String getUserName() {
		return this.UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}
   
}
