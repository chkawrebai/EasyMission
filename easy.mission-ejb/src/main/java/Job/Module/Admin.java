package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity

public class Admin extends User implements Serializable {

	
	private int idAdmin;
	private String FirstName;
	private String LastName;
	private Date DateLastLog;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}   
	public int getIdAdmin() {
		return this.idAdmin;
	}

	public void setIdAdmin(int idAdmin) {
		this.idAdmin = idAdmin;
	}   
	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}   
	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}   
	public Date getDateLastLog() {
		return this.DateLastLog;
	}

	public void setDateLastLog(Date DateLastLog) {
		this.DateLastLog = DateLastLog;
	}
   
}
