package Job.Module;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobSeeker
 *
 */
@Entity

public class JobSeeker extends User implements Serializable {

	
	private int IdSeeker;
	private String FirstName;
	private String LastName;
	private Date DateNaissance;
	private String Sexe;
	
	  @OneToMany(mappedBy = "offres")
	  private Set<Apply> roles = new HashSet<Apply>();

	private static final long serialVersionUID = 1L;

	public JobSeeker() {
		super();
	}   
	public Set<Apply> getRoles() {
		return roles;
	}
	public void setRoles(Set<Apply> roles) {
		this.roles = roles;
	}
	public int getIdSeeker() {
		return this.IdSeeker;
	}

	public void setIdSeeker(int IdSeeker) {
		this.IdSeeker = IdSeeker;
	}   
	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}   
	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}   
	public Date getDateNaissance() {
		return this.DateNaissance;
	}

	public void setDateNaissance(Date DateNaissance) {
		this.DateNaissance = DateNaissance;
	}   
	public String getSexe() {
		return this.Sexe;
	}

	public void setSexe(String Sexe) {
		this.Sexe = Sexe;
	}
   
}
