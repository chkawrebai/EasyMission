package Job.Module;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobProvider
 *
 */
@Entity

public class JobProvider extends User implements Serializable {

	   
	
	private int IdProvider;
	private String companyName;
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL,mappedBy="jobprovider")
	private List<Offre> offres;
	public List<Offre> getOffres() {
		return offres;
	}
	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	private static final long serialVersionUID = 1L;

	public JobProvider() {
		super();
	}   
	public int getIdProvider() {
		return this.IdProvider;
	}

	public void setIdProvider(int IdProvider) {
		this.IdProvider = IdProvider;
	}
   
}
