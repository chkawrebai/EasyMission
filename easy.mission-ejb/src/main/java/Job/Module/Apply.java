package Job.Module;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Apply
 *
 */
@Entity
 
public class Apply implements Serializable {
	private boolean JobDone;
	public boolean isJobDone() {
		return JobDone;
	}
	public void setJobDone(boolean jobDone) {
		JobDone = jobDone;
	}
 
 

	@Id
	@EmbeddedId
	private ApplyId applyIds;


	public ApplyId getApplyIds() {
		return applyIds;
	}
	public void setApplyIds(ApplyId applyIds) {
		this.applyIds = applyIds;
	}



	private Date date_apply;
	private boolean interview;
	private boolean job;
	private int note_Seeker;
	private int note_provider;
	private static final long serialVersionUID = 1L;

	public Apply() {
		super();
	}   
	public Date getDate_apply() {
		return this.date_apply;
	}

	public void setDate_apply(Date date_apply) {
		this.date_apply = date_apply;
	}   
	public boolean getInterview() {
		return this.interview;
	}

	public void setInterview(boolean interview) {
		this.interview = interview;
	}   
	public boolean getJob() {
		return this.job;
	}

	public void setJob(boolean job) {
		this.job = job;
	}   
	public int getNote_Seeker() {
		return this.note_Seeker;
	}

	public void setNote_Seeker(int note_Seeker) {
		this.note_Seeker = note_Seeker;
	}   
	public int getNote_provider() {
		return this.note_provider;
	}

	public void setNote_provider(int note_provider) {
		this.note_provider = note_provider;
	}
   
}
